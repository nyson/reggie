{-# LANGUAGE RecordWildCards #-}
module Main (main) where

import System.Environment
import Text.Reggie.Prelude (pp)
import Text.Reggie
import Text.Reggie.Reduce (reduce)
import Text.Reggie.RegexGenerator (mkGenRegex)

import Test.QuickCheck (generate)
import System.IO (stderr, hPutStrLn)
import System.IO.Error (ioError, userError)
import Control.Monad (foldM, when, forM_)


main :: IO ()
main = do
  Opts{..} <- getArgs >>= pArg fresh
  when (null inputStr) helpText
  forM_ inputStr $ \str -> case parse str of
      Left err    -> hPutStrLn stderr $ "Parse error: " ++ err
      Right regex -> do
        when oParse $ putStrLn $ "Regex regex is: " ++ pp regex
        when oReduce $ putStrLn $ "Reduced regex is: " ++ pp (reduce regex)
        when oGenerate $ case mkGenRegex regex of
          Left err -> hPutStrLn stderr $ "Generator error: " ++ err
          Right gen -> generate gen >>= putStrLn . ("Example string is: " ++)

helpText :: IO ()
helpText = mapM_ putStrLn
  [ "Usage: reggie [OPTION]... [INPUTS]..."
  , ""
  , "reggie reads regexes, reduces them and generates example strings of regexes"
  , ""
  , "\t-r\tReduce regex"
  , "\t-p\tParse regex"
  , "\t-g\tGenerate an example string"
  , ""
  , "Examples:"
  , "\treggie -g \"a*\" \"b*\"\t\t  Generates examples of the given regexes"
  , "\treggie -r \"[a-cd-fg-z][a-mn-z]*\"  Tries to reduce the given regex to a smaller form"
  , ""]

data Opts = Opts
  { oParse :: Bool
  , oReduce :: Bool
  , oGenerate :: Bool
  , inputStr :: [String]
  }

fresh :: Opts
fresh = Opts False False False []

pArg :: Opts -> [String] -> IO Opts
pArg opts (('-':os):xs) = do
  let readOpt :: Opts -> Char -> IO Opts
      readOpt o c = case c of
        'p' -> return o{oParse=   True}
        'g' -> return o{oGenerate=True}
        'r' -> return o{oReduce=  True}
        _   -> ioError . userError $ "Option "++ show c ++" does not exist"
  opts' <- foldM readOpt opts os
  pArg opts' xs

pArg opts (s:xs) = pArg opts{inputStr= s:inputStr opts} xs

pArg opts [] = return opts
