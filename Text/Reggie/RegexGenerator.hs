{-# LANGUAGE LambdaCase #-}
module Text.Reggie.RegexGenerator where

import Text.Reggie.AST
import Text.Reggie.Prelude
import Test.QuickCheck (Gen, oneof, listOf, choose)
import Control.Monad (mapM)
import Control.Monad.Except (ExceptT, replicateM)

type ReggexInput = String
type RGen = ExceptT String Gen ()

rxGen :: Regex -> Gen String
rxGen = either error id . mkGenRegex

mkGenRegex :: Regex -> Either String (Gen String)
mkGenRegex = (Right . oneof =<<) . mapM mkGenStream . flatten

mkGenStream :: RegexStream -> Either String (Gen String)
mkGenStream (RegexStream rs) = fmap concat . sequence <$> mapM mkGenTerm rs

mkGenTerm :: RegTerm -> Either String (Gen String)
mkGenTerm = \case
  TChar c    -> return . return $ c:""
  TScope r   -> mkGenRegex r
  TEscaped e -> mkGenEscaped e
  TCharset negated cs -> mkGenCharset negated cs
  TRep t start end -> mkGenRepetitions t start end

mkGenEscaped :: REscaped -> Either String (Gen String)
mkGenEscaped = return . return . \case
  Escaped ch -> [ch]
  esc        -> pp esc

mkGenRepetitions :: RegTerm -> Int -> Maybe Int -> Either String (Gen String)
mkGenRepetitions term low mhigh = do
  tGen <- mkGenTerm term
  Right $ (mconcat .) . (<>)
    <$> replicateM low tGen
    <*> case mhigh of
          Nothing  -> listOf tGen
          Just top -> sequence . flip replicate tGen =<< choose (0, top - low)

mkGenCharset :: Bool -> [CharsetItem] -> Either String (Gen String)
mkGenCharset True  = const $ Left "negated charset not supported yet"
mkGenCharset False = \case
  [] -> Left "An empty charset will never match a string"
  xs@(_:_) -> oneof <$> mapM mkGenCharsetItem xs

mkGenCharsetItem :: CharsetItem -> Either String (Gen String)
mkGenCharsetItem = \case
  SSpan a b -> return $ (:"") <$> choose (a, b)
  SChar c -> return $ return (c:"")
