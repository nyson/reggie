{-# LANGUAGE LambdaCase, FlexibleInstances, CPP #-}
module Text.Reggie.Parser (parse, unsafeParse) where

import Text.Reggie.AST
import Text.Reggie.Prelude

import Data.Functor  (($>))
import Data.Void     (Void)
import Data.List     (intercalate, sort)
import Control.Monad (when)
import Control.Monad.Combinators.Expr (makeExprParser, Operator(..))

import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.List.NonEmpty as NE
import qualified Data.Set as S

pFail :: String -> Parser ()
pFail = fancyFailure . S.singleton . ErrorFail

type Parser = Parsec Void String

prettifyErrors :: ParseErrorBundle String Void -> NonEmpty String
prettifyErrors errors = showParseError <$> bundleErrors errors
  where
    showParseError :: ParseError String Void -> String
    showParseError (TrivialError a b c)
      = let ppErrItem = \case
              EndOfInput  -> "end of input"
              Label chs   -> "label '" ++ NE.toList chs ++ "'"
              Tokens toks -> "tokens " ++ (intercalate ", "
                                           . map (\t -> '\'':t:"'")
                                           $ NE.toList toks)
            combErrorSet = intercalate ", " . map (\t -> '\'':t:"'")
                           . concatMap (\case Tokens tks -> NE.toList tks
                                              _ -> []) . S.toList
        in show a ++ ": "
           ++ case b of
                Just ts -> "Unexpected " ++ ppErrItem ts ++ "; "
                Nothing -> "No input; "
           ++ "expected " ++ combErrorSet c
    showParseError (FancyError a b) = show a ++ ": " ++ show b

{-# DEPRECATED unsafeParse "Use QuasiQuoting available in Text.Reggie.QQ instead!" #-}
unsafeParse :: String -> Regex
unsafeParse str = case parse str of
  Left  err -> error err
  Right res -> res

parse :: String -> Either String Regex
parse input = first (concat . prettifyErrors) $ M.parse regex "" input

regex :: Parser Regex
regex = makeExprParser (Single <$> stream) ops
  where
    -- ops :: [[ Operator Parser Regex ]]
    ops = [[ binary "|" Union ]]
    binary :: String -> (a -> a -> a) -> Operator Parser a
    binary n f = InfixL $ f <$ L.symbol (return ()) n

stream :: Parser RegexStream
stream = RegexStream <$> many (try term)

regexChar :: Parser Char
regexChar = do
    ch <- printChar
    when (ch `elem` "[(|)]") (pFail $ "tried to parse nonescaped reserved char " ++ show ch)
    return ch

term :: Parser RegTerm
term = repetitions =<< choice
  [ char '.' $> TCharset False [SSpan 'a' 'z']
  , parens <?> "Scoped regular expression"
  , charsetTerm <?> "Charset term"
  , toCharacterClass <$> (TEscaped <$> escaped) <?> "Escaped character"
  , TChar <$> regexChar
  ]

toCharacterClass :: RegTerm -> RegTerm
toCharacterClass (TEscaped (Escaped 'd')) = TCharset False [SSpan '0' '9']
toCharacterClass (TEscaped (Escaped 's')) = TCharset False (map SChar [' '])
toCharacterClass (TEscaped (Escaped 'w')) = TCharset False
                                            [ SSpan '0' '9'
                                            , SSpan 'a' 'z'
                                            , SSpan 'A' 'Z']
toCharacterClass t = t

parens :: Parser RegTerm
parens = TScope <$> (char '(' *> regex <* char ')')

charsetTerm :: Parser RegTerm
charsetTerm = label "charset" $ choice
  [ TCharset True  <$> cst (string "[^")
  , TCharset False <$> cst (char '[')
  ]
  where cst prefix = prefix *> many charsetItem <* char ']'

hex :: Int -> Parser String
hex i = label (show i ++ " hexadecimal characters") $ hx i
 where hx 0   = return "" :: Parser String
       hx len = hexDigitChar >>= \h -> fmap (h:) (hx (len - 1))

escaped :: Parser REscaped
escaped = label "escaped char" $ char '\\' *> choice
  [ Unicode <$> (char' 'u' *> hex 4)
  , Latin   <$> (char' 'x' *> hex 2)
  , Control <$> (char' 'c' *> letterChar)
  , Escaped <$> printChar
  ]

repetitions :: RegTerm -> Parser RegTerm
repetitions rterm = label "repetition (*, +, ? and {n,m})" $ choice
  [ char '*' $> TRep rterm 0 Nothing
  , char '+' $> TRep rterm 1 Nothing
  , char '?' $> TRep rterm 0 (Just 1)
  , do
      atLeast <- char '{' *> space *> L.decimal <* space
      atMost <- choice
        [ try $ char ','
          *> space
          *> choice [try $ Just <$> L.decimal, pure Nothing]
          <* space
          <* char '}'
        , space
          *> char '}'
          $> Just atLeast
        ]
      return $ TRep rterm atLeast atMost
  , return rterm
  ]

charsetItem :: Parser CharsetItem
charsetItem = choice
  [ try $ SSpan <$> regexChar <*> (char '-' *> regexChar)
  , try $ SChar <$> regexChar
  ]
