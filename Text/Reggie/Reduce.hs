{-# LANGUAGE FlexibleInstances, LambdaCase #-}
-- | Reduces regexes to a minimal variant
module Text.Reggie.Reduce where

import Text.Reggie.AST

import Data.List (sort)
import Control.Applicative (liftA2)

class Reductible a where
  reduce :: a -> a

instance Reductible REscaped where
  reduce = id

instance Reductible Regex where
  reduce = reduceRegex

instance Reductible RegexStream where
  reduce = reduceStream

instance Reductible RegTerm where
  reduce = \case
    TRep t minB maxB -> TRep (reduce t) minB maxB
    TScope r -> TScope (reduce r)
    TCharset neg cs -> case (neg, reduceCharsetItems cs) of
      (False, [SChar c]) -> TChar c
      (_neg, cs')        -> TCharset neg cs'
    rt -> rt

instance Reductible [RegTerm] where
  reduce = reduceTerms

instance Reductible [CharsetItem] where
  reduce = reduceCharsetItems

reduceStream :: RegexStream -> RegexStream
reduceStream (RegexStream rstr) = RegexStream $ reduceTerms rstr

reduceRegex :: Regex -> Regex
reduceRegex (Union r1 r2) = case (reduceRegex r1, reduceRegex r2) of
  (r1', r2')
    | r1' == r2' -> r1'
    | otherwise  -> Union r1' r2'
reduceRegex (Single rs) = Single $ reduceStream rs

charsetItemToChar :: CharsetItem -> String
charsetItemToChar (SChar ch)  = [ch]
charsetItemToChar (SSpan a b) = [a..b]

reduceTerms :: [RegTerm] -> [RegTerm]
reduceTerms = rts . map reduce
  where rts (l:r:next) = case (l,r) of
          -- Combining repetitions of the same regex
          (TRep tl i1 mb1, TRep tr i2 mb2)
            | tl == tr -> rts (TRep tl (i1 + i2) (liftA2 (+) mb1 mb2): next)
          -- Combine a regex and a repetition of the same regex
          (tl, TRep trep i mb)
            | tl == trep -> rts (TRep trep (i+1) ((+1) <$> mb):next)
          -- As above but regex on the right
          (TRep trep i mb, tr)
            | trep == tr -> rts (TRep trep (i+1) ((+1) <$> mb):next)
          -- Combines two equal regexes
          (tl, tr)
            | tl == tr -> rts (TRep tl 2 (Just 2): next)
          _ -> l:rts (r:next)
        rts (a:next) = a: rts next
        rts [] = []

reduceCharsetItems :: [CharsetItem] -> [CharsetItem]
reduceCharsetItems = rci . sort
  where
    rci (x:y:next) = case nubber x y of Just z  -> rci (z:next)
                                        Nothing -> x: rci (y:next)
      where nubber :: CharsetItem -> CharsetItem -> Maybe CharsetItem
            nubber l r = case (l, r) of
              (SChar a, SChar b)
                | a == b    -> Just l
              (SSpan a1 a2, SSpan b1 b2)
                | a1 <= b1 && b2 <= a2 -> Just l
                | b1 <= a1 && a2 <= b2 -> Just r
                | succ a2 == b1        -> Just (SSpan a1 b2)
              (SChar ch, SSpan a b)
                | a <= ch && ch <= b -> Just r
              (SSpan a b, SChar ch)
                | a <= ch && ch <= b -> Just l
              _ -> Nothing
    rci (x:next) = x:rci next
    rci []       = []
