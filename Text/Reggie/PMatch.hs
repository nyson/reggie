{-# LANGUAGE LambdaCase #-}
module Text.Reggie.PMatch
  ( matchPart
  , matchRegex
  , matchStream
  ) where

import Text.Reggie.AST
import Text.Reggie.Prelude

import Text.Megaparsec
import Text.Megaparsec.Char

import Control.Monad (replicateM_, unless, when)
import Control.Monad.Identity (Identity)
import Data.Maybe (maybe)
import Data.Void ()

import qualified Data.Set as Set

data MatchError
  = Mismatch String
  | Unsalvageable String
  deriving (Ord, Eq, Show)

instance ShowErrorComponent MatchError where
  showErrorComponent = show
  errorComponentLen = const (-1)

-- | Matcher Monad
-- | Wraps the Parser with custom errors, using String as buffer
type Matcher = ParsecT MatchError String Identity -- (S.State MatchState)

parseExactly :: Stream s => ParsecT e s m a -> Int -> ParsecT e s m [a]
parseExactly parser n
  | n <= 0    = return []
  | otherwise = (:) <$> parser <*> parseExactly parser (n-1)

matchPart :: Matcher () -> String -> Either String ()
matchPart psr input = first show $ runParser psr "Matching Regex" input

next :: Matcher Char
next = printChar

nextN :: Int -> Matcher String
nextN = parseExactly printChar

mismatch :: String -> Matcher ()
mismatch = fancyFailure . Set.singleton . ErrorCustom . Mismatch

matchRegex :: Regex -> Matcher ()
matchRegex = label "Regex" . choice . map matchStream . flatten

matchStream :: RegexStream -> Matcher ()
matchStream (RegexStream terms) = label "Stream" . ms . collapseReps $ terms
  where ms :: [[RegTerm]] -> Matcher ()
        ms [] = return ()
        ms (ts:nx) = case ts of
          []      -> ms nx
          [term]  -> matchTerm term   *> ms nx
          repTerm -> foldThem repTerm *> ms nx

        foldThem [] = return ()
        foldThem [t] = matchTerm t
        foldThem (TRep t a b:t2) = matchRepetitions t (Just $ foldThem t2) a b
        foldThem stream = error $ "Expecting stream starting with repetition, got "
                          ++ pp (RegexStream stream)
        isRep TRep{} = True
        isRep _      = False

        collapseReps :: [RegTerm] -> [[RegTerm]]
        collapseReps [] = []
        collapseReps ts = case span isRep ts of
          (reps, [])     -> [reps]
          (reps, a:nx) -> (reps ++ [a]):collapseReps nx

matchTerm :: RegTerm -> Matcher ()
matchTerm = \case
  TChar c -> matchChar c
  TCharset negated csitems -> matchCharset negated csitems
  TRep t minB maxB -> matchRepetitions t Nothing minB maxB
  TScope r -> matchRegex r
  TEscaped e -> matchEscaped e

mismatchUnless :: String -> Bool -> Matcher ()
mismatchUnless msg cond = unless cond $ mismatch msg

mismatchWhen :: String -> Bool -> Matcher ()
mismatchWhen msg cond = when cond $ mismatch msg

matchChar :: Char -> Matcher ()
matchChar ch = label ("Matching char " ++ show ch)
  $ next >>= mismatchWhen (show $ TChar ch) . (/= ch)

matchEscaped :: REscaped -> Matcher ()
matchEscaped esc = label ("Matching escaped char '" ++ pp esc ++ "'")
  $ char '\\' *> case esc of
      Escaped ch  -> next >>= mismatchUnless (show esc) . (== ch)
      Latin   hex -> char 'x' *> nextN 2 >>= mismatchUnless (show esc) . (== hex)
      Unicode hex -> char 'u' *> nextN 4 >>= mismatchUnless (show esc) . (== hex)
      Control ch  -> char 'c' *> next    >>= mismatchUnless (show esc) . (== ch)

matchRepetitions :: RegTerm -> Maybe (Matcher ()) -> Int -> Maybe Int -> Matcher ()
matchRepetitions t t2 minB maxB = label lab $ do
  replicateM_ minB mt
  perhapsMatch maxB mt
  where lab = concat ["Match at least ", show minB
                     , maybe "" (\b -> " and at most " ++ show b) maxB
                     , " repetitions of regex ", pp t
                     ]
        mt = matchTerm t
        perhapsMatch :: Maybe Int -> Matcher () -> Matcher ()
        perhapsMatch (Just 0) _ = fromMaybe (return ()) t2
        perhapsMatch i m = choice [ try m *> perhapsMatch ((\x -> x-1) <$> i) m
                                  , try $ fromMaybe (return ()) t2]

matchCharset :: Bool -> [CharsetItem] -> Matcher ()
matchCharset negated cs = label "Charset" $ choice
  $ map (try . matchCharsetItem negated) cs


matchCharsetItem :: Bool -> CharsetItem -> Matcher ()
matchCharsetItem negated ci = label ("Charset item " ++ pp ci) $ do
  ch <- next
  case ci of
    SSpan mini maxi
      | mini > maxi
        -> error $ "Bad regex, " ++ show mini ++ " is larger than "
           ++ show maxi ++ " in the charset item " ++ pp ci
      | negated `xor` (ch `elem` [mini .. maxi])
        -> return ()
    SChar sch
      | negated `xor` (sch == ch)
        -> return ()
    _ -> mismatch (show ci)
