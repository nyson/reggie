{-# LANGUAGE GADTs, LambdaCase, DeriveGeneric, RecordWildCards, DeriveDataTypeable #-}
module Text.Reggie.AST where

import Test.QuickCheck
import Text.Reggie.Prelude
import Control.Monad (replicateM)
import Data.Data

-- A disjoint stream of regexes
data Regex = Union Regex Regex
           | Single RegexStream
  deriving (Show, Read, Data)

flatten :: Regex -> [RegexStream]
flatten = \case
  Union l r -> flatten l ++ flatten r
  Single r  -> [r]

instance Eq Regex where
  r1 == r2 = flatten r1 == flatten r2

instance Arbitrary Regex where
  arbitrary = genRegex GROpts{grAllowEmpty= True}

newtype GenRegexOpts = GROpts
  { grAllowEmpty :: Bool
  }

genRegex :: GenRegexOpts -> Gen Regex
genRegex opts@GROpts{..} =
  frequency
    [ ( 10, if grAllowEmpty
            then Single <$> arbitrary
            else Single . RegexStream <$> listOf1 arbitrary
      )
    , ( 1, Union
           <$> scaled (genRegex opts{grAllowEmpty= False})
           <*> scaled (genRegex opts{grAllowEmpty= False})
      )
    ] where scaled = scale (`div` 2)

instance PrettyPrint Regex where
  pp = \case
    Union left right -> pp left <> "|" <> pp right
    Single rstr -> pp rstr


-- A conjunctive stream of regex terms
newtype RegexStream = RegexStream [RegTerm]
  deriving (Show, Eq, Read, Generic, Data)

unRegexStream :: RegexStream -> [RegTerm]
unRegexStream (RegexStream terms) = terms

instance Arbitrary RegexStream where
  shrink = genericShrink
  arbitrary = RegexStream <$> listOf arbitrary

instance PrettyPrint RegexStream where
  pp (RegexStream rstr) = concatMap pp rstr


-- An item in a regex stream
data RegTerm
  = TScope Regex
  | TChar Char
  | TEscaped REscaped
  | TRep RegTerm Int (Maybe Int)
  | TCharset Bool [CharsetItem]
  deriving (Show, Eq, Read, Data)

genRepetition :: RegTerm -> Gen RegTerm
genRepetition term = uncurry (TRep term) <$> oneof
  [ (\i -> (i, Just i)) <$> genPositiveInteger
  , return (0, Nothing)
  , return (1, Nothing)
  , return (1, Just 1)
  , (\a b -> (a, (+a) <$> b))
    <$> genPositiveInteger
    <*> oneof [ return Nothing
              , Just <$> genPositiveInteger]
  ]

instance Arbitrary RegTerm where
  arbitrary = do
    let scaleF = (round :: Double -> Int) . sqrt . fromIntegral
    term <- frequency
      [ (3,   TScope   <$> scale scaleF (genRegex GROpts{grAllowEmpty= True}))
      , (100, TChar    <$> genValidChar)
--      , (5,   TScope . Single . RegexStream . (:[]) . TEscaped <$> arbitrary)
      , (10,  TCharset <$> arbitrary <*> listOf1 arbitrary)
      ]
    frequency
      [ (10, return term)
      , (1, genRepetition term)
      ]

instance PrettyPrint RegTerm where
  pp = \case
    TChar c -> c:""
    TScope r -> concat ["(", pp r, ")"]
    TEscaped e -> pp e
    TRep t 0 Nothing  -> pp t ++ "*"
    TRep t 1 Nothing  -> pp t ++ "+"
    TRep t 0 (Just 1) -> pp t ++ "?"
    TRep t n Nothing -> concat
      [ pp t , "{", show n , ",}"]
    TRep t n (Just m)
      | m == n -> pp t ++ "{" ++ show n ++ "}"
      | otherwise -> concat
        [ pp t
        , "{", show n, ","
        , show m, "}"
        ]
    TCharset negated csis -> concat
      [ "["
      , if negated then "^" else mempty
      , concatMap pp csis
      , "]"
      ]

-- Escaped chars
data REscaped where
  Escaped :: Char   -> REscaped
  Latin   :: String -> REscaped
  Unicode :: String -> REscaped
  Control :: Char   -> REscaped
  deriving (Show, Eq, Read, Data)

instance Arbitrary REscaped where
  arbitrary = oneof
    [ Escaped <$> elements ".\\[{}]"
    -- , Latin   <$> genHexUpper 2
    -- , Unicode <$> genHexUpper 4
    -- , Control <$> genLetterChar
    ]

instance PrettyPrint REscaped where
  pp = \case
    Escaped c  -> '\\':[c]
    Latin   hx -> "\\x" ++ hx
    Unicode hx -> "\\u" ++ hx
    Control hx -> '\\':'c':hx:""

-- | An item in a charset
data CharsetItem
  = SSpan Char Char
  | SChar Char
  deriving (Eq, Show, Read, Data)

charsetItemRange :: Char -> Char -> Maybe CharsetItem
charsetItemRange c1 c2
  | c1 < c2 = Just $ SSpan c1 c2
  | otherwise = Nothing


{-
SSpan is greater than SChar

Ord instance for spans that matches
         |a=====b|            --
         |a'===b'|            EQ
    |a'===b'|                 LT
|a'===b'|                     LT
           |a'b'|             LT
            |a'===b'|         GT
                   |a'===b'|  GT
       |a'=======b'|          GT

-}
instance Ord CharsetItem where
  compare (SChar _) (SSpan _ _)     = LT
  compare (SSpan _ _) (SChar _)     = GT
  compare (SChar a) (SChar b)       = a `compare` b
  compare (SSpan a b) (SSpan a' b')
    | a == a' && b == b' = EQ
    | a' > a  && b' < b  = GT
    | b' > b             = LT
    | a' < a             = GT
    | otherwise = EQ

instance PrettyPrint CharsetItem where
  pp = \case
    SSpan start end -> concat [start:"", "-", end:""]
    SChar c -> c:""

instance Arbitrary CharsetItem where
  arbitrary = oneof
    [ SChar <$> genValidChar
    , (\case [a,b] -> SSpan (min a b) (max a b)
             _     -> error "Linear types would have caught this!"
      ) <$> replicateM 2 (elements ['a'..'z'])
    ]
