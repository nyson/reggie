{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Text.Reggie.Prelude
  ( genPositiveInteger, genHex, genHexUpper
  , genValidChar, genLetterChar
  , safeTake, count, xor, decr
  , NonEmpty(..), PrettyPrint(..)
  , StringConv(..), Set()
  , module Data.Bifunctor
  , module GHC.Generics
  , module Data.Maybe
  ) where

import Control.Applicative (liftA2)
import Test.QuickCheck (Gen(..), elements, arbitrary)
import Data.Maybe
import Data.List.NonEmpty hiding (xor)
import Data.Set
import GHC.Generics
import Data.Bifunctor

class PrettyPrint a where
  pp :: a -> String

instance PrettyPrint Char where
  pp c = [c]

class StringConv a b where
  conv :: a -> b

instance StringConv String String where
  conv = id

safeTake :: Int -> [a] -> Maybe [a]
safeTake 0 _ = Just []
safeTake _ [] = Nothing
safeTake n (x:xs) = (x:) <$> safeTake (n-1) xs

count :: Monad m => Int -> m a -> m [a]
count 0 _  = return []
count n mf = (:) <$> mf <*> count (n-1) mf

decr :: Int -> Int
decr x = x-1

genValidChar :: Gen Char
genValidChar = elements ['a'..'z']--chr <$> elements valids

genLetterChar :: Gen Char
genLetterChar = elements $ ['a'..'z'] ++ ['A'..'Z']

-- | Generates a positive integer
genPositiveInteger :: Gen Int
genPositiveInteger = abs <$> arbitrary

-- | generates a sized of hex values
genHex :: Int -> Gen String
genHex _size
  | _size <= 0 = return ""
  | otherwise = liftA2 (:) (elements hexChars) (genHex $ _size - 1)
  where hexChars = ['a' .. 'f'] ++ ['A' .. 'F'] ++ ['0' .. '9']

genHexUpper :: Int -> Gen String
genHexUpper _size
  | _size <= 0 = return ""
  | otherwise = liftA2 (:) (elements hexChars) (genHex $ _size - 1)
  where hexChars = ['A' .. 'F'] ++ ['0' .. '9']



xor :: Bool -> Bool -> Bool
xor True False = True
xor False True = True
xor _ _        = False
