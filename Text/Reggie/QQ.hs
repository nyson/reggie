module Text.Reggie.QQ where

import Text.Reggie.Parser (parse)
import Language.Haskell.TH.Quote (QuasiQuoter(..))
import Language.Haskell.TH.Syntax (liftData)

rx :: QuasiQuoter
rx = QuasiQuoter
  { quoteExp  = either error liftData . parse
  , quotePat  = error "No patterns available for regex"
  , quoteType = error "No types declared for regex"
  , quoteDec  = error "No declarations for regex"
  }
