{-# LANGUAGE FlexibleContexts #-}

module Text.Reggie
  ( parse, sampleReg, generator, rxGen, rx, Regex, pp
  , genRegex, GenRegexOpts(..))
  where

import Text.Reggie.Parser (parse)
import Text.Reggie.AST (Regex, genRegex, GenRegexOpts(..))
import Text.Reggie.RegexGenerator (mkGenRegex, rxGen)
import Text.Reggie.QQ
import Text.Reggie.Prelude (pp)
import Data.String

import Test.QuickCheck (generate, Gen)

instance IsString Regex where
  fromString = either error id . parse


generator :: String -> Gen String
generator str = either error id
  $ parse str >>= mkGenRegex

sampleReg :: String -> IO String
sampleReg = generate . generator
