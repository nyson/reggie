{-# LANGUAGE OverloadedStrings, QuasiQuotes, FlexibleInstances #-}
module ParserTest where

import Test.Tasty.QuickCheck (counterexample, Property, (===), forAll, discard)
import Test.Tasty.HUnit (assertEqual, Assertion)

import Text.Reggie.Prelude (pp)
import Text.Reggie.AST (Regex(..), RegexStream(..))

import Text.Reggie.Reduce (reduce)
import Text.Reggie.RegexGenerator (mkGenRegex)
import Text.Reggie.Parser (parse)
import Text.Reggie.QQ (rx)

import Text.RE.TDFA.String ((*=~), compileRegex, anyMatches)
import Data.Either (either)
import Control.Monad (unless)

instance MonadFail (Either String) where
  fail = Left

prop_parseRegex :: Regex -> Property
prop_parseRegex rx = counterexample
  (    "\nRegex:      " ++ pp rx
    ++ "\nSimplified: " ++ (pp . reduce) rx
    ++ "\nSimpl. AST: " ++ (show . reduce) rx
    ++ "\n"
  ) $ parse (pp rx) === Right rx

prop_matchOtherRegex :: Regex -> Property
prop_matchOtherRegex rx = case mkGenRegex rx of
  Left _err -> discard
  Right gen -> counterexample
    (    "\nGenerator: '" ++ pp rx ++ "'"
      ++ "\nReduced:   '" ++ (pp . reduce) rx ++ "'"
      ++ "\nRedu. AST:  " ++ (show . reduce) rx
      ++ "\n"
    ) $ forAll gen $ \str -> matchToOther rx str === Right ()

unit_quasiQuotes :: Assertion
unit_quasiQuotes = assertEqual "Regex in QuasiQuotes match"
  (parse "a*")
  (Right [rx|a*|])

unit_digitCharClass :: Assertion
unit_digitCharClass = assertEqual "\\d equals a digit"
  (parse "\\d")
  (parse "[0-9]")

unit_wsCharClass :: Assertion
unit_wsCharClass = assertEqual "\\s equals a whitespace"
  (parse "\\s")
  (parse "[ ]")

-- | Order currently matters here, is that really the test we want?
unit_wordCharClass :: Assertion
unit_wordCharClass = assertEqual "\\w equals a word char"
  (parse "\\w")
  (parse "[0-9a-zA-Z]")

matchToOther :: Regex -> String -> Either String ()
matchToOther rx str = do
  cRx <- compileRegex $ case rx of
    Single (RegexStream []) -> "()"
    neReg -> pp neReg
  let matches = str *=~ cRx
  unless (anyMatches matches)
    . Left $ "Mismatch: " ++ show matches
